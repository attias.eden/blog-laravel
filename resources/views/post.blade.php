@extends('layouts.app')

@section('content')
<form method="POST">
        @csrf
<div class="container">
  <div class="card">
    <div class="card-body ">
      <div class="row">
        <h4 class="d-inline  card-title col-11">{{$post->title}}</h4>
        <button type="submit"  value="like_button" name="like_button"  class="btn btn-primary col-1">
         Like <span class="badge badge-light">{{$post->likes_count}}</span>
        </button>
      </div>
   <hr>
      <div class="card-body">
        <h6 class="card-subtitle mb-2 text-muted">{{$post->content}}</h6>

    @auth
       @if (Auth::user()->is_dev)
    <p class="card-text">{{$post->technical_content}}</p>
       @endif
   @endauth

   {{-- @foreach($tags as $tag)
      <span class="badge badge-primary">{{$tag->name}}</span>
    @endforeach

   --}}
  </div>
</div>

</div>
   </form>
@endsection
