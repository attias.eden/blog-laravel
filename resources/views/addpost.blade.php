@extends('layouts.app')

@section('content')


<div class="container">
    <form action="/home" method="POST">
        @csrf
<div class="form-group">

     <h3 for="title">Add a post</h3>
     @error('title')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
      <input type="text" name='title' class="form-control" id="title" aria-describedby="emailHelp" placeholder="Enter the title">
<br>
@error('content')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
      <textarea class="form-control" name='content' id="content" rows="7" placeholder="Enter the content"></textarea>
 <br>
 @error('technical_content')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
      <textarea class="form-control" name='technical_content'  id="technical_content" rows="7" placeholder="Enter the technical content"></textarea>

    </div>
    <br>
    <div class="form-group">
            <h5 >Select a tag</h5>
           
            @foreach ($tags as $tag)
            <div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" name="tag_checkbox[]" value="{{$tag->id}}">
  <label class="form-check-label" >{{$tag->name}}</label>
</div>
@endforeach

<div class="input-group mb-3">
  <input type="text" name="new_tag" class="form-control col-md-2" placeholder="Enter a new tag" aria-label="new tag" aria-describedby="button-addon2">
  <div class="input-group-append">
    <button class="btn btn-outline-secondary" type="button" id="button-addon2">Add </button>
  </div>
</div>

    </div>

    <br>
    <input type="submit" class="btn btn-primary" value="Add the post">
</form>
    </div>
@endsection
