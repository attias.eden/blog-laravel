<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::group(['middleware' => ['auth']], function () {
       Route::get('/addpost', 'PostController@addPost');
       Route::get('/post/{title}', 'PostController@postView');
       Route::post('/post/{title}', 'PostController@incremLikes');
    //   Route::get('/home', 'PostController@postByTag');
       Route::post('/home', 'PostController@savePost');
  


});
