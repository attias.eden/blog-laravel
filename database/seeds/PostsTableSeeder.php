<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $posts = [
        [
            'title' => "Post 1",
            'content' => "This is the post 1.",
            'technical_content' => "Technical content 1",
            'likes_count' => 2,

        ],

        [
            'title' => "Post 2",
        'content' => "This is the post 2.",
        'technical_content' => "Technical content 2",
        'likes_count' => 4,
        ],

        [
            'title' => "Post 3",
            'content' => "This is the post 3.",
            'technical_content' => "",
            'likes_count' => 0,
        ],
    ];

    DB::table('posts')->insert($posts);
}

}
