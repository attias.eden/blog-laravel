<?php

use Illuminate\Database\Seeder;

class PostTagTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $posts = App\Post::all();
        $tags = App\Tag::all();
       // Populate the pivot table
        $posts->each(function ($post) use ($tags) { 
        $post->tags()->attach(
        $tags->random(rand(1, 3))->pluck('id')->toArray()
    ); 
});
    }
}
