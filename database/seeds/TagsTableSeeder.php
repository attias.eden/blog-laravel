<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       
        $tags = [
            [
                'name' => "tag1",
            ],
    
            [
                'name' => "tag2",
            ],
    
            [ 
                'name' => "tag3",
            ],
        ];
    
        DB::table('tags')->insert($tags);
    }
    
}
