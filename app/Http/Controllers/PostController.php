<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\Console\Input\Input;

class PostController extends Controller
{


    public function addPost(){

    $tags=  DB::table('tags')->get();
       return view('addpost',['tags'=> $tags]);
    }

    public function postView(){

        $post = DB::table('posts')->where('title',request()->title)->first();
        $tags= DB::table('post_tag')->where('post_id',$post->id)->get();
        //dd(DB::table('post_tag')->get());
        return view('post',['post'=> $post, 'tags'=>$tags]);
    }

    public function  savePost(){

             $validator = Validator::make(request()->all(), [
            'title' => 'required|unique:posts|max:255',
            'content' => 'required|min:10',
            'technical_content' => 'required|min:10',
        ]);

      
        if ($validator->fails()) {
            return redirect('addpost')
                        ->withErrors($validator)
                        ->withInput();
        }

        // there is not errors

        $post= Post::create([
            'title'=>request('title'),
            'content'=>request('content'),
            'technical_content'=>request('technical_content'),
            'likes_count'=>0,
        ]);

        $tags_id_checked= request()->input('tag_checkbox');
        foreach($tags_id_checked as $tag_id){
           $post->tags()->attach($tag_id);}
        return redirect('home'); 
        }

    public function incremLikes(Request $request){

    $post = DB::table('posts')->where('title',request()->title)->first();
    //dd($request);
       if($request->get('like_button')) { 
          DB::table('posts')
            ->where('id', $post->id)
            ->update(['likes_count' => $post->likes_count + 1]);
        }
   return back();
}
}


