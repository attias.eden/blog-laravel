<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
        'title','content','technical_content','likes_count',
    ];

    public function tags(){
        return $this->belongsToMany(Tag::class, 'post_tag');
    }
}
